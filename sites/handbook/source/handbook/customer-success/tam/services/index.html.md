---
layout: handbook-page-toc
title: "TAM Responsibilities and Services"
description: "There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## TAM Alignment

For an overview of TAM segments by ARR, please see [this wiki page](https://gitlab.com/gitlab-com/customer-success/tam/-/wikis/Segments)(Internal GitLab only)

## Responsibilities and Services

There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab and their utilization of GitLab's products and services. These services typically include the following. Please note this list is not definitive, and more services may be provided than listed or some may not be offered, depending on the size and details of the account.

### Relationship Management

- Regular cadence calls
  - Ask customers about planned upgrades on a regular basis. Refer them to the [Live Upgrade Assistance page](/support/scheduling-live-upgrade-assistance.html#how-do-i-schedule-live-upgrade-assistance) so that they have a plan in place (including rollback strategy) and give Support enough preparation time to be available to help.
- Regular open issue reviews and issue escalations
- Account health checks
- Executive business reviews (1-2 times a year for Enterprise; as needed for Commercial)
- Success strategy roadmaps - beginning with an onboarding success plan, for example a 30/60/90 day plan
- To act as a key point of contact for guidance and advice and as a liaison between the customer and other GitLab teams
- Own, manage, and deliver the customer onboarding experience
- Help GitLab's customers realize the value of their investment in GitLab
- GitLab Days

### Training

- Identification of pain points and training required
- Coordination of demos and training sessions, potentially delivered by the Technical Account Manager if time and technical knowledge allows
- "Brown Bag" trainings
- Regular communication and updates on GitLab features
- Product and feature guidance - new feature presentations

### Support

- Upgrade planning
- User adoption strategy
- Migration strategy and planning
- [Infrastructure upgrade coordination](/handbook/customer-success/tam/services/infrastructure-upgrade/)
- Launch support
- Monitors support tickets and ensures that the customer receives the appropriate support levels
- Support ticket escalations
- Monitor SaaS based customer experience by adding them to the [Marquee Accounts alerts](https://gitlab.com/gitlab-com/gl-infra/marquee-account-alerts) project
